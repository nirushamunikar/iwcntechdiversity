from django.conf.urls import url,include
from django.contrib import admin
from myapp import views

urlpatterns = [
    url(r'^logout/$',views.user_logout,name="logout"),
    url(r'^register/$', views.register, name="register"),
    url(r'^user_login/$', views.user_login, name="user_login"),
    url(r'^form_demo/$', views.form_demo, name="form_demo"),
]