from django import forms
from django.contrib.auth.models import User
from django.core import  validators
from .models import UserProfileInfo


class UserForm(forms.ModelForm):
    password=forms.CharField(widget=forms.PasswordInput())
    class Meta:
        model=User
        fields=('username','email','password')

class UserProfileInfoForm(forms.ModelForm):
    class Meta:
        model=UserProfileInfo
        fields=('portfolio_site',
                )
def name_validator(value):
    if len(value) <=5:
        raise forms.ValidationError("The length must be greater than 5")

class FormDemo(forms.Form):
    name=forms.CharField(validators=[name_validator])
    email=forms.EmailField()
    verify_email=forms.EmailField(label="Enter your email again")
    contact=forms.CharField()
    address=forms.CharField()
    new=forms.CharField(required=False,widget=forms.HiddenInput,validators=[validators.MaxLengthValidator(0)])

    def clean_new(self): #=clean only one field
        new=self.cleaned_data['new']
        if len(new)>0:
            raise forms.ValidationError("Error")
        return new

    def clean(self):
        all_clean_data= super().clean()
        email=all_clean_data['email']
        vemail=all_clean_data['verify_email']

        if email!= vemail:
            raise forms.ValidationError("both email must watch")



