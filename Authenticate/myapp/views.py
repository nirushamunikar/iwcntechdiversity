from django.shortcuts import render,redirect
from django.contrib.auth import authenticate,login,logout
from .forms import UserForm,UserProfileInfoForm
from django.http import HttpResponseRedirect,HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
def index(request):
    return render(request,'myapp/index.html')

def register(request):
    registered=False
    if request.method=="POST":
        user_form=UserForm(data=request.POST) #value haru aafai assign hunxa ;charfield ho ki haina check grxa is_valid le
        profile_form=UserProfileInfoForm(data=request.POST)

        if user_form.is_valid() and  profile_form.is_valid():
            user=user_form.save() #user=>
            user.set_password(user.password) #password convert into hashvalue
            user.save()
            profile=profile_form.save(commit=False) #relation save gareko xaina "user" so database ma napurayeko
            profile.user=user  #profile.user chai mathi ko user ho vaneko
            profile.save()
            print("sucessfully registered user")
            registered = True

        else:
            print("Invalid form request")

    else:
        user_form=UserForm()
        profile_form=UserProfileInfoForm()

    return render(request,'myapp/registration.html',{
            'user_form':user_form,
            'profile_form':profile_form,
            'registered':registered
        })

def user_login(request):
    if request.method=="POST":
        username=request.POST.get('username') #donot take null value
        password= request.POST.get('password')
        user=authenticate(username=username,password=password)
        if user:
            #logged in condition
            if user.is_active:
                login(request,user) #creating session for logged in user
                return HttpResponseRedirect(reverse('index'))
            else:
                return HttpResponse("your account is not active")
        else:
            return HttpResponse("invalid login details")

    else:
        return render(request,'myapp/login.html',{})

@login_required #login required garena vane chai /logout garyo vane tyo page ma janxa so first ma wu logged in vayo vane matra logout ma jana milxa
def user_logout(request):
    logout(request) #session is destroyed
    status=1
    return HttpResponseRedirect(reverse('index'))

from . import forms

def form_demo(request):
    form=forms.FormDemo()

    if request.method=="POST":
        form=forms.FormDemo(request.POST)

        if form.is_valid():
            return HttpResponse("the form is valid")

    return render(request,'myapp/form_demo.html',{'form':form})

